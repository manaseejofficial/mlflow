import os
from mlflow import log_metric, log_param, log_artifact

import mlflow as mf




#mlflow run --experiment-name 'TEST4_LOCAL_TO_MLFLOW' --version cb24e3  https://gitlab.com/manaseejofficial/mlflow.git

if __name__ == "__main__":
    mf.set_tracking_uri("http://34.72.136.72:5000")
    mf.projects.run("https://gitlab.com/manaseejofficial/mlflow.git", version="2fe713", experiment_name="TEST4_LOCAL_TO_MLFLOW", use_conda=True)
